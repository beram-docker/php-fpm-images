#!/usr/bin/env bash

set -euo pipefail

usage() {
    printf "\n"
    printf "Usage:\n\t%s [OPTIONS] EXT\n" "$0"
    printf "\n"
    printf "Argument:\n"
    printf "\tEXT : the extension to enable.\n"
    printf "\n"
    printf "Options:\n"
    printf "\\t-h : Display this help\\n"
    printf "\\t-l : List available extensions\\n"
    printf "\n"
    exit 0
}

_list_available_extensions() {
    echo "The available extensions are:"
    declare -F | awk '{print $NF}' | grep -E "^enable_ext_" | sed "s/^enable_ext_/  /g"
}

enable_ext_pcov() {
  local -r file="${1}"
  echo 'extension=pcov.so' > "${file}"
}

enable_ext_xdebug() {
  local -r file="${1}"
  echo 'zend_extension=xdebug.so' > "${file}"
}

if [ -z "${1+x}" ] || [ "$1" = '-h' ]; then
    usage
fi

if [[ "$1" = '-l' ]]; then
    _list_available_extensions
    exit 0
fi

readonly EXT="$1"
readonly PHP_DOCKER_INI="${PHP_INI_DIR}/conf.d/docker-php-ext-${EXT}.ini"
readonly CHOSEN_EXTENSION="enable_ext_${EXT}"

if declare -f "${CHOSEN_EXTENSION}" > /dev/null; then
    # Execute the chosen extension.
    ${CHOSEN_EXTENSION} "${PHP_DOCKER_INI}"
else
    printf "'%s' is not a known extension.\\n" "${EXT}"
    printf "\\n"
    _list_available_extensions
    usage
fi
