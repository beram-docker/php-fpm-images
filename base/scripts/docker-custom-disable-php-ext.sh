#!/usr/bin/env bash

set -eu

usage() {
    printf "\n"
    printf "Usage:\n\t%s [OPTIONS] EXT\n" "$0"
    printf "\n"
    printf "Argument:\n"
    printf "\tEXT : the extension to disable.\n"
    printf "\n"
    printf "Options:\n"
    printf "\\t-h : Display this help\\n"
    printf "\n"
    exit 0
}

if [ -z "${1+x}" ] || [ "$1" = '-h' ]; then
    usage
fi

readonly EXT="$1"
readonly PHP_DOCKER_INI="${PHP_INI_DIR}/conf.d/docker-php-ext-${EXT}.ini"

rm -f "${PHP_DOCKER_INI}"
