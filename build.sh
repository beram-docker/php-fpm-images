#!/usr/bin/env bash

set -ex

readonly IMAGE_TO_BUILD="$CI_REGISTRY/beram-docker/php-fpm-images/$IMAGE_NAME:$IMAGE_VERSION"

# If the image already exists do not build it.
docker manifest inspect "$IMAGE_TO_BUILD" && exit 0
docker buildx build --platform=linux/amd64,linux/arm64 -f "$IMAGE_NAME/Dockerfile" -t "$IMAGE_TO_BUILD" --build-arg "BASE_IMAGE_VERSION=$BASE_IMAGE_VERSION" "$IMAGE_NAME/" --push
